<?php

/**
 * @file
 * solr_nutch module file.
 */

/**
 * Implements hook_search_info().
 */
function solr_nutch_search_info() {
  $settings = variable_get('solr_nutch', array());
  return array(
    'title' => isset($settings['tab_name']) ? check_plain($settings['tab_name']) : 'Solr Nutch',
    'path' => 'solr',
  );
}

/**
 * Implements hook_permission().
 */
function solr_nutch_permission() {
  return array(
    'administer solr nutch' => array(
      'title' => t('Administer Solr Nutch'),
      'description' => t('Edit Solr Nutch settings.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function solr_nutch_menu() {
  $items = array();
  $items['admin/config/search/nutch'] = array(
    'title' => 'Solr Nutch settings',
    'description' => 'Manages settings for the Solr Nutch module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('solr_nutch_admin_form'),
    'access arguments' => array('administer solr nutch'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Admin form function for the module.
 */
function solr_nutch_admin_form() {
  $settings = variable_get('solr_nutch', array());
  $form = array();
  $form['#tree'] = TRUE;
  $form['solr_nutch'] = array(
    '#title' => t('Solr Nutch'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['solr_nutch']['host'] = array(
    '#title' => t('Hostname of Solr server'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['host']) ? $settings['host'] : 'localhost',
    '#description' => 'Type the hostname of the Solr server (defaults to <em>localhost</em>).',
  );
  $form['solr_nutch']['port'] = array(
    '#title' => t('TCP port of Solr server'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['port']) ? $settings['port'] : '8983',
    '#description' => 'Type the port of the Solr server (defaults to <em>8983</em>).',
  );
  $form['solr_nutch']['path'] = array(
    '#title' => t('Solr core path on Solr server'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['path']) ? $settings['path'] : '/solr/',
    '#description' => 'The patch to the Solr core (defaults to <em>/solr/</em>).',
  );
  $form['solr_nutch']['rows'] = array(
    '#title' => t('Rows per result page'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['rows']) ? $settings['rows'] : '10',
    '#description' => 'How many rows should be displayed on each result page (defaults to <em>10</em>).',
  );
  $form['solr_nutch']['tab_name'] = array(
    '#title' => t('Search results tab name'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['tab_name']) ? $settings['tab_name'] : 'Solr Nutch',
    '#description' => 'Enter a custom name of the tab where search results are displayed (defaults to <em>Solr Nutch</em>).',
  );
  return system_settings_form($form);
}

/**
 * Implements hook_search_execute().
 */
function solr_nutch_search_execute($keys = NULL) {
  $query = isset($keys) ? $keys : FALSE;
  if ($query) {
    $answers = array();
    $settings = variable_get('solr_nutch', array());
    $limit = isset($settings['rows']) ? check_plain($settings['rows']) : 10;
    $host = isset($settings['host']) ? check_plain($settings['host']) : 'localhost';
    $port = isset($settings['port']) ? check_plain($settings['port']) : 8983;
    $path = isset($settings['path']) ? check_plain($settings['path']) : '/solr/';
    $offset = isset($_REQUEST['page']) ? check_plain($_REQUEST['page']) * $limit : 0;
    $parameters = array(
      'hl' => 'true',
      'hl.highlightMultiTerm' => 'true',
      'hl.simple.pre' => '<strong>',
      'hl.simple.post' => '</strong>',
      'hl.fl' => 'content',
    );
    $solr = new Apache_Solr_Service($host, $port, $path);

    if (!$solr->ping()) {
      drupal_set_message(t('Error contacting the Apache Solr search server. Check your settings at admin/config/search/nutch.'), 'error');
      return;
    }

    // If magic quotes is enabled then stripslashes will be needed.
    if (get_magic_quotes_gpc() == 1) {
      $query = stripslashes($query);
    }

    try {
      $results = $solr->search(urlencode($query), $offset, $limit, $parameters);
    }
    catch (Exception $e) {
      drupal_set_message(t("@e.", array('@e' => $e->__toString())), 'error');
    }
    if ($results) {
      global $_solr_nutch_count, $_solr_nutch_pages;
      $_solr_nutch_count = (int) $results->response->numFound;
      $_solr_nutch_pages = ceil($_solr_nutch_count / $limit);
      foreach ($results->response->docs as $document) {
        $id = $document->id;
        $answer = array();
        $answer['type'] = 'node';
        $answer['title'] = !is_null($document->title) ? $document->title : $document->url;
        $answer['link'] = $document->url;
        $answer['snippet'] = '';
        if (isset($results->highlighting->$id->content)) {
          $answer['snippet'] = '...';
          foreach ($results->highlighting->$id->content as $highlight) {
            $answer['snippet'] .= $highlight . '... ';
          }
          $answer['snippet'] .= '<br />';
        }
        $answer['snippet'] .= l($document->url, $document->url);
        $answers[] = $answer;
      }
    }
    return ($answers);
  }
}

/**
 * Implements hook_search_page().
 */
function solr_nutch_search_page($results) {
  global $_solr_nutch_count, $_solr_nutch_pages;
  pager_default_initialize($_solr_nutch_pages, 1, $element = 0);
  $resultcount = $_solr_nutch_count <> 1 ? $_solr_nutch_count . ' results.' : $_solr_nutch_count . ' result.';
  $output['prefix']['#markup'] = '';
  $output['prefix']['#markup'] = '<p>Your search returned ' . $resultcount . '</p>';
  $output['prefix']['#markup'] .= '<ol class="search-results">';

  if ($results) {
    foreach ($results as $entry) {
      $output[] = array(
        '#theme' => 'search_result',
        '#result' => $entry,
        '#module' => 'solr_nutch',
      );
    }
  }
  $output['suffix']['#markup'] = '</ol>' . theme('pager', array('quantity' => 9));

  return $output;
}
